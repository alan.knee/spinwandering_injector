#!/usr/bin/env python

import numpy as np
import subprocess
import os, argparse
from sdeint import itoint
from helper_functions import parse_inj_ini

class FreqTrackMaker:
    def __init__(self, freqs_init_dic, timing_init_dic, extra_arguments, which):
        self.tsft = int(timing_init_dic['tsft'])
        self.ttotal = int(timing_init_dic['ttotal'])
        self.nsft = np.floor(self.ttotal / self.tsft).astype(int)
        self.tcoh = int(timing_init_dic['tcoh'])
        self.ncoh = np.floor(self.ttotal / self.tcoh).astype(int)
        self.nsft_per_coh = np.floor(self.tcoh / self.tsft).astype(int)

        self.f0_init = float(freqs_init_dic['f0_init'])
        self.f1_init = float(freqs_init_dic['f1_init'])
        self.f2_init = float(freqs_init_dic['f2_init'])
        self.phase_init = float(freqs_init_dic['phase_init'])

        # can set a seed for reproducibility, this is not the seed used by MFD
        if 'seed' in extra_arguments:
            self.seed = int(extra_arguments['seed'])
        elif which == 'det':
            self.seed = 0
        else:
            self.seed = np.random.randint(0, int(2**30))
        self.extra_arguments = extra_arguments

        self.which = which

        if self.which in ['uniformf', 'maximalf', 'uniformfddot']:
            if 'wander_scale' not in self.extra_arguments:                
                raise KeyError('Must have wander_scale defined in extra_arguments dic!')
            else:
                self.wander_scale = float(self.extra_arguments['wander_scale'])

        # Ornstein-Uhlenbeck wandering can be generated using either wander_scale (i.e. \Delta f_SW / \Delta f_bin), or sigma
        elif self.which == 'ou':
            if 'gamma' not in self.extra_arguments:
                raise KeyError('Must have both gamma and sigma defined in extra_arguments dic for OU process!')
            elif 'sigma' not in self.extra_arguments and 'wander_scale' in self.extra_arguments:
                self.gamma = float(extra_arguments['gamma'])
                # converts a wander_scale to an equivalent sigma, see Eq. (B4) in paper
                self.sigma = float(self.extra_arguments['wander_scale']) * self.tcoh**(-3/2) / 4
            else:
                self.gamma = float(extra_arguments['gamma'])
                self.sigma = float(extra_arguments['sigma'])

    # helper function calls appropriate functions to compute phase, freq, fdot, fddot evolutions
    def calculate_track(self):
        if self.which=='det':
            p, f, fd, fdd = self.deterministic_evolution()
        elif self.which=='uniformf':
            p, f, fd, fdd = self.uniformfreq_wander()
        elif self.which=='maximalf':
            p, f, fd, fdd = self.maximaluniformfreq_wander()
        elif self.which=='uniformfddot':
            p, f, fd, fdd = self.uniformfddot_wander()
        elif self.which=='ou':
            p, f, fd, fdd = self.oufreq_wander()
        else:
            raise NotImplementedError(f"'which' keyword {self.which} not in accepted range of ['det', 'uniformf', 'maximalf', 'uniformfddot', 'ou']")
        self.phase_seq, self.freq_seq, self.fdot_seq, self.fddot_seq = p, f, fd, fdd
        return self.phase_seq, self.freq_seq, self.fdot_seq, self.fddot_seq

    # if no wandering, just return deterministic quantities
    def deterministic_evolution(self, return_justfreq=False):
        times = np.arange(0, self.ttotal, step=self.tsft)
        phase_seq = self.phase_init + self.f0_init * times + self.f1_init / 2 * times**2 + \
                    self.f2_init / 6 * times**3 
        phase_seq = phase_seq - np.floor(phase_seq) # bound phase to [0, 1)
        freq_seq = self.f0_init + self.f1_init * times + self.f2_init / 2 * times**2
        fdot_seq = self.f1_init + self.f2_init * times
        fddot_seq = np.repeat(self.f2_init, self.nsft)
        
        if return_justfreq:
            return freq_seq
        else:
            return phase_seq, freq_seq, fdot_seq, fddot_seq

    # not used in paper, but picks freq. uniformly between \pm \Delta f_SW 
    def uniformfreq_wander(self, override_seed=False, return_justfreq=False):
        if override_seed is False:
            rng = np.random.default_rng(seed=self.seed)
        else:
            rng = np.random.default_rng(seed=override_seed)
        
        freq_binsize = 1 / (2 * self.tcoh)
        max_wander = self.wander_scale * freq_binsize

        phase = self.phase_init
        freq = self.f0_init
        phase_seq = [phase]
        freq_seq = [freq]

        for i in np.arange(start=1, stop=self.nsft, dtype=int):
            if i % self.nsft_per_coh==0:
                freq = freq + rng.uniform(-1, 1) * max_wander
            freq_seq.append(freq)
            phase = phase + freq * self.tsft
            phase = phase - np.floor(phase)
            phase_seq.append(phase)

        phase_seq = np.array(phase_seq)
        freq_seq = np.array(freq_seq)        
        fdot_seq = np.repeat(self.f1_init, self.nsft)
        fddot_seq = np.repeat(self.f2_init, self.nsft)

        if return_justfreq:
            return freq_seq
        else:
            return phase_seq, freq_seq, fdot_seq, fddot_seq

    # defined as SW-f in paper
    def maximaluniformfreq_wander(self, override_seed=False, return_justfreq=False, offset_freq=True):
        if override_seed is False:
            rng = np.random.default_rng(seed=self.seed)
        else:
            rng = np.random.default_rng(seed=override_seed)
        
        freq_binsize = 1 / (2 * self.tcoh)
        max_wander = self.wander_scale * freq_binsize

        phase = self.phase_init
        # if requested, offset the starting frequency by a fraction of a bin, so that we don't preferentially align
        if offset_freq:
            freq = self.f0_init + rng.uniform(-1, 1) * freq_binsize / 2
        else:
            freq = self.f0_init
        phase_seq = [phase]
        freq_seq = [freq]

        for i in np.arange(start=1, stop=self.nsft, dtype=int):
            if i % self.nsft_per_coh==0:
                freq = freq + rng.choice([-1, 0, 1]) * max_wander
            freq_seq.append(freq)
            phase = phase + freq * self.tsft
            phase = phase - np.floor(phase)
            phase_seq.append(phase)

        phase_seq = np.array(phase_seq)
        freq_seq = np.array(freq_seq)        
        fdot_seq = np.repeat(self.f1_init, self.nsft)
        fddot_seq = np.repeat(self.f2_init, self.nsft)

        if return_justfreq:
            return freq_seq
        else:
            return phase_seq, freq_seq, fdot_seq, fddot_seq

    # defined as SW-\ddot{f} in paper
    def uniformfddot_wander(self, eff_mean_reversion=True, override_seed=False, return_justfreq=False):
        if override_seed is False:
            rng = np.random.default_rng(seed=self.seed)
        else:
            rng = np.random.default_rng(seed=override_seed)

        freq_binsize = 1 / (2 * self.tcoh)
        max_wander = self.wander_scale * freq_binsize

        phase = self.phase_init
        freq = self.f0_init
        fdot = self.f1_init
        fddot = self.f2_init
        phase_seq = [phase]
        freq_seq = [freq]
        fdot_seq = [fdot]
        fddot_seq = [fddot]

        for i in np.arange(start=0, stop=self.nsft-1, dtype=int):
            if i % self.nsft_per_coh == 0:
                if eff_mean_reversion and fdot > 0:
                    fddot = 2 / self.tcoh**2 * rng.uniform(-max_wander, max_wander - fdot * self.tcoh)
                elif eff_mean_reversion and fdot <= 0:
                    fddot = 2 / self.tcoh**2 * rng.uniform(-max_wander - fdot * self.tcoh, max_wander)
                else:
                    fddot = 2 / self.tcoh**2 * max_wander * rng.uniform(-1, 1)
            phase = freq * self.tsft + fdot / 2 * self.tsft**2 + fddot / 6 * self.tsft**3 + phase
            phase = phase - np.floor(phase) # bound phase to [0, 1)
            freq = freq + fdot * self.tsft + fddot / 2 * self.tsft**2
            fdot = fdot + fddot * self.tsft

            phase_seq.append(phase)
            freq_seq.append(freq)
            fdot_seq.append(fdot)
            fddot_seq.append(fddot)

        phase_seq = np.array(phase_seq)
        freq_seq = np.array(freq_seq)        
        fdot_seq = np.array(fdot_seq)
        fddot_seq = np.array(fddot_seq)

        if return_justfreq:
            return freq_seq
        else:
            return phase_seq, freq_seq, fdot_seq, fddot_seq

    # defined as SW-OU in paper
    def oufreq_wander(self, override_seed=False, return_justfreq=False):
        if override_seed is False:
            rng = np.random.default_rng(seed=self.seed)
        else:
            rng = np.random.default_rng(seed=override_seed)

        tspan = np.linspace(self.tsft, self.ttotal, num=self.nsft, endpoint=True)

        f = lambda x, t: -self.gamma * (x - self.f0_init)
        g = lambda x, t: self.sigma
        freq_seq = np.ravel(itoint(f, g, self.f0_init, tspan, generator=rng))

        phase = self.phase_init
        phase_seq = [phase]
        for freq in freq_seq[1:]:
            phase = phase + freq * self.tsft
            phase = phase - np.floor(phase)
            phase_seq.append(phase)

        phase_seq = np.array(phase_seq)
        fdot_seq = np.repeat(self.f1_init, self.nsft)
        fddot_seq = np.repeat(self.f2_init, self.nsft)

        if return_justfreq:
            return freq_seq
        else:
            return phase_seq, freq_seq, fdot_seq, fddot_seq

    # save all relevant computed values, and initial params to a file, for later reconstruction
    def save_to_file(self, path_to_dir, filename_prepend='ftrack'):
        descriptor_string = f'% f0_init: {self.f0_init}, f1_init: {self.f1_init}, f2_init: {self.f2_init}, phase_init: {self.phase_init}' + '\n'
        descriptor_string += f'% tsft: {self.tsft}, ttotal: {self.ttotal}, tcoh: {self.tcoh}' + '\n'
        descriptor_string += f'% seed: {self.seed}' + '\n'
        extras_string = f'% which: {self.which}, '
        for key, val in self.extra_arguments.items():
            if key != 'seed':
                extras_string += f'{key}: {val}, '
        extras_string += '\n'
        descriptor_string += extras_string
        descriptor_string += f'% PHASE, FREQ, FDOT, FDDOT' + '\n'

        self.calculate_track()
        path_string = ''
        for phase, freq, fdot, fddot in zip(self.phase_seq, self.freq_seq, self.fdot_seq, self.fddot_seq):
            path_string += f'{phase}, {freq}, {fdot}, {fddot}' + '\n'

        full_string = descriptor_string + path_string

        file_name = f'{filename_prepend}-{self.which}.txt'

        with open(os.path.join(path_to_dir, file_name), 'w+') as f:
            f.writelines(full_string)

def parse_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--which",
        help="Which wandering process to use",
        default=None,
        type=str,
    )
    parser.add_argument(
        "--ws",
        help="wander_scale (\mathcal{W} in paper)",
        default=1,
        type=float,
    )
    parser.add_argument(
        "--save_dir",
        help="Directory to save frequency path",
        default='$JOBFS/path',
        type=str,
    )
    parser.add_argument(
        "--save_prefix",
        help="Prefix to add to saved path file, before -which.txt",
        default='ftrack',
        type=str,
    )
    parser.add_argument(
        "--tcoh",
        help="Coherence time, used to compute \Delta f_SW",
        default=86400,
        type=int,
    )
    parser.add_argument(
        "--ttotal",
        help="Total time",
        default=86400*100,
        type=int,
    )
    parser.add_argument(
        "--gamma",
        help="gamma parameter for the OU wandering",
        default=None,
        type=float,
    )
    parser.add_argument(
        "--sigma",
        help="sigma parameter for the OU wandering",
        default=np.nan,
        type=float,
    )
    parser.add_argument(
        "--inj_ini",
        help="Full path to injection ini file",
        default=None,
        type=str,
    )
    args = parser.parse_args()
    return args

if __name__=='__main__':
    cl_params = parse_command_line()
    start, sqrtSx, search_fband, freqs_init_dic, timing_init_dic, source_dic, binary_dic = parse_inj_ini(cl_params.inj_ini)

    if cl_params.which=='det':
        extra_arguments = dict()
    elif cl_params.which in ['uniformf', 'maximalf', 'uniformfddot']:
        extra_arguments = dict(wander_scale=cl_params.ws)
    elif cl_params.which=='ou':
        if np.isnan(cl_params.sigma):
            extra_arguments = dict(gamma=cl_params.gamma, wander_scale=cl_params.ws)
        else:    
            extra_arguments = dict(gamma=cl_params.gamma, sigma=cl_params.sigma)

    Maker = FreqTrackMaker(freqs_init_dic, timing_init_dic, extra_arguments, cl_params.which)
    if not os.path.isdir(cl_params.save_dir):
        subprocess.run(f'mkdir -p {cl_params.save_dir}', shell=True)
    Maker.save_to_file(cl_params.save_dir, cl_params.save_prefix)
