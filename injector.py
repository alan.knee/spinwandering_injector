#!/usr/bin/env python

import numpy as np
import subprocess
import argparse
from freq_track_maker import FreqTrackMaker
from helper_functions import parse_inj_ini, unpack_binary_dic, unpack_source_dic

def parse_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path_file",
        help="Full path to pathmaker file",
        default=None,
        type=str,
        required=True,
    )
    parser.add_argument(
        "--inj_ini",
        help="Full path to injection ini file",
        default=None,
        type=str,
        required=True,
    )
    parser.add_argument(
        "--h0",
        help="h0 value to inject, if h0 is not in inj_ini",
        default=np.nan,
        type=float,
    )
    parser.add_argument(
        "--out_dir",
        help="Path to output final sft(s) into",
        default='sfts',
        type=str,
        required=True,
    )
    parser.add_argument(
        "--temp_dir",
        help="Path to temporarily store interim SFTs",
        default='temp_sfts',
        type=str,
        required=False,
    )
    parser.add_argument(
        "--noise_sfts",
        help="SFT files for passing straight to MFD_v5",
        default=None,
        type=str,
        required=False,
    )
    parser.add_argument(
        "--print_only",
        help="Flag to only print commands, not run them",
        type=bool,
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "--clean_temp",
        help="Flag to rm temporary 1800s SFTs after collating them",
        type=bool,
        default=True,
        action=argparse.BooleanOptionalAction,
    )
    args = parser.parse_args()
    return args

def construct_maker_from_file(path_to_file):
    with open(path_to_file, 'r') as f:
        all_lines = f.readlines()
    
    freqs_dic = dict([l.strip('% ').split(': ') for l in all_lines[0].strip('\n').split(',')])

    timing_dic = dict([l.strip('% ').split(': ') for l in all_lines[1].strip('\n').split(',')])

    seed_read = int(all_lines[2].split(':')[1])
    extra_arguments = dict(seed=seed_read)

    extras = all_lines[3].strip('\n').split(',')
    if len(extras[-1].strip())==0:
        extras = extras[:-1]
    which = extras[0].split(':')[1].strip()
    if len(extras) > 1:
        for extra in extras[1:]:
            keyval = extra.split(': ')
            extra_arguments.update({keyval[0].strip(): keyval[1]})

    Maker = FreqTrackMaker(freqs_dic, timing_dic, extra_arguments, which)
    return Maker

def inject_path_into_noise(cl_params):
    out_dir = cl_params.out_dir
    PathMaker = construct_maker_from_file(cl_params.path_file)
    start, sqrtSx, search_fband, freqs_init_dic, timing_init_dic, source_dic, binary_dic = parse_inj_ini(cl_params.inj_ini)
    h0, cosi, alpha, delta, psi = unpack_source_dic(source_dic)
    # override h0 from ini if provided on command line
    if not np.isnan(cl_params.h0): 
        h0 = cl_params.h0
    if binary_dic:
        asini, period, tasc = unpack_binary_dic(binary_dic)

    phase_seq, freq_seq, fdot_seq, fddot_seq = PathMaker.calculate_track()
    
    assert PathMaker.nsft == len(freq_seq)

    # (constant) minimum frequency of the band
    # TODO: read this from ini if provided, rather than inferring from desired search band
    # TODO: also make generated band size a passed param (c.f. line 139)
    fmin = PathMaker.f0_init - 2 * search_fband

    # if injecting an actually wandering signal, must create separate tsft length SFTs
    if PathMaker.which != 'det':
        # temporary directory to keep the tsft length sfts before combining
        if not cl_params.print_only:
            if cl_params.temp_dir != 'None':
                temp_dir = cl_params.temp_dir
            else:
                temp_dir = f'{out_dir}/temp'
            subprocess.run(f'mkdir -p {temp_dir}', shell=True)
        else:
            print(f'mkdir -p {out_dir}/temp')
            temp_dir = f'{out_dir}/temp'

        loop_start = start
        # if not ou wandering can generate chunks of SFTs every tcoh, much faster
        if 'ou' not in PathMaker.which:
            tcoh = PathMaker.tcoh
            nsft_per_coh = PathMaker.nsft_per_coh
            phase_seq = phase_seq[::nsft_per_coh]
            freq_seq = freq_seq[::nsft_per_coh]
            fdot_seq = fdot_seq[::nsft_per_coh]
            fddot_seq = fddot_seq[::nsft_per_coh]
            duration = tcoh
        else:
            duration = PathMaker.tsft
        for phase, freq, fdot, fddot in zip(phase_seq, freq_seq, fdot_seq, fddot_seq):
            # MFD_v5 command to make each SFT
            if binary_dic:
                inj_sources_str = (
                                f"{{h0={h0};cosi={cosi};refTime={loop_start};"
                                f"Alpha={alpha};Delta={delta};psi={psi};"
                                f"Freq={freq};f1dot={fdot};f2dot={fddot};phi0={phase*2*np.pi};"
                                f"orbitTp={tasc-period/4};orbitasini={asini};orbitPeriod={period}}}"
                                )
            else:
                inj_sources_str = (
                                f"{{h0={h0};cosi={cosi};refTime={loop_start};"
                                f"Alpha={alpha};Delta={delta};psi={psi};"
                                f"Freq={freq};f1dot={fdot};f2dot={fddot};phi0={phase*2*np.pi}}}"
                                )
            # inject into Gaussian noise if no noise_sfts provided
            if cl_params.noise_sfts != 'None':
                mfd_cmd = (
                        f"lalpulsar_Makefakedata_v5 "
                        f"--outSingleSFT=TRUE "
                        f"--outSFTdir={temp_dir} "
                        f"--fmin={fmin} "
                        f"--IFOs=\"H1,L1\" "
                        f"--sqrtSX={sqrtSx} "
                        f"--startTime={loop_start} "
                        f"--duration={duration} "
                        f"--Band={search_fband*4} "
                        f"--Tsft=1800 "
                        f"--SFTWindowType=\"Tukey\" "
                        f"--SFTWindowParam=0.001 "
                        f"--injectionSources=\"{inj_sources_str}\" "
                        )
            # otherwise inject into provided sfts
            else:
                mfd_cmd = (
                        f"lalpulsar_Makefakedata_v5 "
                        f"--outSingleSFT=TRUE "
                        f"--outSFTdir={temp_dir} "
                        f"--IFOs=\"H1,L1\" "
                        f"--startTime={loop_start} "
                        f"--duration={duration} "
                        f"--Tsft=1800 "
                        f"--injectionSources=\"{inj_sources_str}\" "
                        f"--noiseSFTs={cl_params.noise_sfts} "
                        )
            loop_start += duration
            if not cl_params.print_only:
                subprocess.run(mfd_cmd, shell=True)
            else:
                print(mfd_cmd)

        # concatenate all the generated SFTs together, reducing future I/O costs
        # TODO: make this optional, and transfer SFTs to final dir if undesired
        cat_H1_join_cmd = (
                          f"cat "
                          f"{temp_dir}/H-*_H1_*.sft "
                          f"> {out_dir}/H-{PathMaker.nsft}_H1_1800SFT_mfdv5-{start}-{PathMaker.ttotal}.sft"
                          )
        cat_L1_join_cmd = (
                          f"cat "
                          f"{temp_dir}/L-*_L1_*.sft "
                          f"> {out_dir}/L-{PathMaker.nsft}_L1_1800SFT_mfdv5-{start}-{PathMaker.ttotal}.sft"
                          )
        rm_singles_dir_cmd = f"rm -rf {temp_dir}"
        if not cl_params.print_only:
            subprocess.run(cat_H1_join_cmd, shell=True)
            subprocess.run(cat_L1_join_cmd, shell=True)
            if cl_params.clean_temp:
                subprocess.run(rm_singles_dir_cmd, shell=True)
        else:
            print(cat_H1_join_cmd)
            print(cat_L1_join_cmd)
            if cl_params.clean_temp:
                print(rm_singles_dir_cmd)

    # else if injecting a deterministic signal, can just do one call to MFD
    else:
        f0 = freq_seq[0]
        f1 = fdot_seq[0]
        f2 = fddot_seq[0]
        phase = phase_seq[0]
        if binary_dic:
            inj_sources_str = (
                            f"{{h0={h0};cosi={cosi};refTime={start};"
                            f"Alpha={alpha};Delta={delta};psi={psi};"
                            f"Freq={f0};f1dot={f1};f2dot={f2};phi0={phase*2*np.pi};"
                            f"orbitTp={tasc-period/4};orbitasini={asini};orbitPeriod={period}}}"
                            )
        else:
            inj_sources_str = (
                            f"{{h0={h0};cosi={cosi};refTime={start};"
                            f"Alpha={alpha};Delta={delta};psi={psi};"
                            f"Freq={f0};f1dot={f1};f2dot={f2};phi0={phase*2*np.pi}}}"
                            )
        mfd_cmd = (
                f"lalpulsar_Makefakedata_v5 "
                f"--outSingleSFT=TRUE "
                f"--outSFTdir={out_dir} "
                f"--fmin={fmin} "
                f"--IFOs=\"H1,L1\" "
                f"--sqrtSX={sqrtSx} "
                f"--startTime={start} "
                f"--duration={PathMaker.ttotal} "
                f"--Band={search_fband*4} "
                f"--Tsft=1800 "
                f"--SFTWindowType=\"Tukey\" "
                f"--SFTWindowParam=0.001 "
                f"--injectionSources=\"{inj_sources_str}\" "
                )
        if not cl_params.print_only:
            subprocess.run(mfd_cmd, shell=True)
        else:
            print(mfd_cmd)

if __name__=='__main__':
    cl_params = parse_command_line()
    inject_path_into_noise(cl_params)
