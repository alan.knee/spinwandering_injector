import configparser
import numpy as np

def parse_inj_ini(inj_ini_file):
    config = configparser.ConfigParser()
    config.read(inj_ini_file)
    
    source = config['source_params']
    detector = config['detector_params']
    try:
        binary = config['binary_source_params']
        isbinary = True
    except:
        isbinary = False

    try:
        h0 = float(source['h0'])
    except:
        h0 = np.nan
    
    freqs_init_dic = dict(f0_init=float(source['f0_init']), f1_init=float(source['f1_init']), f2_init=float(source['f2_init']), phase_init=float(source['phase_init']))
    timing_init_dic = dict(tsft=int(detector['tsft']), tcoh=int(detector['tcoh']), ttotal=int(detector['ttotal']))
    source_dic = dict(h0=h0, alpha=float(source['alpha']), delta=float(source['delta']), psi=float(source['psi']), cosi=float(source['cosi']))
    if isbinary:
        binary_dic = dict(asini=float(binary['asini']), period=float(binary['period']), tasc=float(binary['tasc']))
    else:
        binary_dic = {}

    start = int(detector['start'])
    sqrtSx = detector['sqrtSx']
    search_fband = float(detector['search_fband'])

    return start, sqrtSx, search_fband, freqs_init_dic, timing_init_dic, source_dic, binary_dic

def unpack_source_dic(source_dic):
    return source_dic['h0'], source_dic['cosi'], source_dic['alpha'], source_dic['delta'], source_dic['psi']

def unpack_binary_dic(binary_dic):
    return binary_dic['asini'], binary_dic['period'], binary_dic['tasc']

